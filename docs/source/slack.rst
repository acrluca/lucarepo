Slack
=====

We use Slack to communicate among the group. You can join the Slack workspace with your AEI email address through this link:

- Join the Slack workspace: `<https://mpg-aei.slack.com>`_

Once you have joined the Slack you can browse the list of channels and join the ones you think are relevant for you. You can also send private messages to anyone in the group and start audio or video calls with them.

It is recommended to use the Slack desktop app (`<https://slack.com/download>`_) and keep it open throughout the working day to stay in contact with the group.
k
