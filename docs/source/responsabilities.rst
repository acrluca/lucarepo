.. _responsabilities:

Responsibilities
================

The following table lists the people responsible for various things in the division.

.. list-table::
    :header-rows: 1

    * - Area
      - Responsible parties
      - Description 	
    * - Clusters
      - Steffen Grunewald 
      - Administering HPC and HTC clusters at AEI Potsdam	
    * - Mailing list
      - Nils Fischer 
      - Maintaning the ACR mailing list 	
    * - Room bookings
      - Brit Holland
      - Booking rooms for meetings, etc	
    * - Weekly ACR seminars
      - | Michael Katz, Justin Vines
        | & Miguel Zumalacarregui
      - | Inviting speakers, announcing them 
        | and hosting them 	
    * - AEI Colloquia
      - | Stephen Green 
        | (& M.P. Heller, K. Kiuchi)
      - Organizing the AEI colloquia 	
    * - Wiki 
      - Serguei Ossokine
      - Giving access to people, general maintenance	
    * - Weekly ACR group meetings
      - | Andrea Antonelli, Riccardo Barbieri 
        | & Mohammed Khalil
      - | Creating schedule for arxiv and paper
        | reader. Maintaining list of papers to read 	

