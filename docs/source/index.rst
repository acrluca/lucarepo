Welcome to ACR computer facilities documentation!
=================================================

.. toctree::
    :caption: Communication Tools
    
    Slack <slack.rst>
    Mailing Lists <mailing.rst>
    Responsabilities <responsabilities.rst>

.. toctree::
    :caption: Meeting and Conferences
    


