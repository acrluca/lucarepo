Mailing Lists
=============

The following mailing lists may be of interest to members of the division:

.. list-table::
    :header-rows: 1

    * - List
      - Purpose
    * - `astro-cosmo-rel <https://lists.aei.mpg.de/mailman/listinfo/astro-cosmo-rel>`_
      - General discussion and announcements (includes visitors)
    * - `astro-cosmo-rel-members <https://lists.aei.mpg.de/mailman/listinfo/astro-cosmo-rel-members>`_
      - General discussion and announcements (excludes visitors)
    * - `astro-cosmo-rel-lsc <https://lists.aei.mpg.de/mailman/listinfo/astro-cosmo-rel-lsc>`_
      - | Discussions and announcements for members of the LIGO Scientific 
        | Collaboration
    * - `nr-p <https://lists.aei.mpg.de/mailman/listinfo/nr-p>`_
      - Numerical relativity
    * - `minerva-users <https://lists.aei.mpg.de/mailman/listinfo/minerva-users>`_
      - Minerva cluster 
    * - `vulcan-users <https://lists.aei.mpg.de/mailman/listinfo/Vulcan-users>`_
      - Vulcan cluster 

To join or manage your subscription, click the link in the table above to go to the list information page.

The `Responsibilities <responsabilities>`_ page lists the person currently responsible for managing the mailing lists. 
